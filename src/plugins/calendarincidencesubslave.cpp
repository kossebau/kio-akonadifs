/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "calendarincidencesubslave.h"

// Akonadi
#include <Akonadi/ItemFetchJob>
#include <Akonadi/ItemFetchScope>
// KF
#include <KCalendarCore/Attachment>
#include <KIO/UDSEntry>
#include <KIO/Global>
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QDebug>


K_PLUGIN_CLASS_WITH_JSON(CalendarIncidenceSubSlave, "calendarincidencesubslave.json")

namespace {

class AttachmentUri
{
 public:
    enum Type { InvalidUrl, Attachment};
 public:
    explicit AttachmentUri(const QString &path);

 public:
    int index() const { return mIndex; }
    AttachmentUri::Type type() const;
    bool isAttachment() const { return type() == Attachment; }
    bool isValid() const      { return type() != InvalidUrl; }

private: // data
    int mIndex = -1;
};

AttachmentUri::AttachmentUri(const QString &path)
{
    bool ok = false;
    mIndex = path.toLongLong(&ok);
    if (!ok) {
        mIndex = -1;
    }
}

AttachmentUri::Type AttachmentUri::type() const
{
    return (mIndex < 0) ? InvalidUrl : Attachment;
}

}

CalendarIncidenceSubSlave::CalendarIncidenceSubSlave(QObject *parent, const KPluginMetaData &metaData,
                                                     const QVariantList &args)
    : AkonadiFS::AbstractItemContentSubSlave(parent, metaData, args)
{
}

CalendarIncidenceSubSlave::~CalendarIncidenceSubSlave() = default;

bool CalendarIncidenceSubSlave::supportsItemType(const QString &mimeType) const
{
    return KCalendarCore::Incidence::mimeTypes().contains(mimeType);
}

bool CalendarIncidenceSubSlave::updateMessage(Akonadi::Item::Id itemId)
{
    if (mItemId == itemId) {
        return true;
    }

    const Akonadi::Item item(itemId);
    auto job = new Akonadi::ItemFetchJob(item);
    job->fetchScope().fetchFullPayload();

    if (!job->exec()) {
        error(KIO::ERR_INTERNAL, job->errorString());
        return false;
    }

    if (job->items().size() != 1) {
        error(KIO::ERR_DOES_NOT_EXIST, i18n("No such item."));
        return false;
    }

    const Akonadi::Item fullItem = job->items().at(0);
    mItemId = itemId;
    mIncidence = fullItem.payload<KCalendarCore::Incidence::Ptr>();

    return true;
}

void CalendarIncidenceSubSlave::listDir(const AkonadiFS::Uri &url)
{
    Akonadi::Item::Id itemId = url.itemId();

    if (!updateMessage(itemId)) {
        return;
    }

    const KCalendarCore::Attachment::List attachments = mIncidence->attachments();
    for (int i = 0; i < attachments.size(); ++i) {
        listEntry(entryForAttachment(attachments[i], i));
    }

    finished();
}

void CalendarIncidenceSubSlave::stat(const AkonadiFS::Uri &url)
{
    Akonadi::Item::Id itemId = url.itemId();

    const AttachmentUri attachmentUri(url.itemSubPath());

    if (attachmentUri.isAttachment()) {
        if (!updateMessage(itemId)) {
            return;
        }
        const int index = attachmentUri.index();
        const KCalendarCore::Attachment::List attachments = mIncidence->attachments();
        if (index < attachments.size()) {
            statEntry(entryForAttachment(attachments[index], index));
            finished();
            return;
        }
    }
    error(KIO::ERR_DOES_NOT_EXIST, i18n("No such attachment."));
}

void CalendarIncidenceSubSlave::get(const AkonadiFS::Uri &url)
{
    Akonadi::Item::Id itemId = url.itemId();

    const AttachmentUri attachmentUri(url.itemSubPath());

    if (attachmentUri.isAttachment()) {
        if (!updateMessage(itemId)) {
            return;
        }
        const int index = attachmentUri.index();
        const KCalendarCore::Attachment::List attachments = mIncidence->attachments();
        if (index < attachments.size()) {
            const KCalendarCore::Attachment &attachment = attachments[index];
            if (attachment.isBinary()) {
                mimeType(attachment.mimeType());
                data(attachment.decodedData());
                finished();
                return;
            }
        }
    }

    error(KIO::ERR_DOES_NOT_EXIST, i18n("No such attachment."));
}

KIO::UDSEntry CalendarIncidenceSubSlave::entryForAttachment(const KCalendarCore::Attachment &attachment, int index)
{
    KIO::UDSEntry entry;
    entry.reserve(6);

    entry.fastInsert(KIO::UDSEntry::UDS_NAME, QString::number(index));
    entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, attachment.label());
    entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, attachment.mimeType());
    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFREG);
//     entry.fastInsert(KIO::UDSEntry::UDS_SIZE, attachment->size()); // TODO, should we force decoding?
    if (attachment.isUri() && !attachment.isBinary()) { // TODO: it can be both? prefer local copy then
        entry.fastInsert(KIO::UDSEntry::UDS_TARGET_URL, attachment.uri());
    }
    entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR | S_IRGRP | S_IROTH);
//     entry.fastInsert(KIO::UDSEntry::UDS_MODIFICATION_TIME, item.modificationTime().toSecsSinceEpoch());
    return entry;
}

#include "calendarincidencesubslave.moc"
