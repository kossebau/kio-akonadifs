/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef MESSAGERFC822SUBSLAVE_H
#define MESSAGERFC822SUBSLAVE_H

// AkonadiFS
#include <AkonadiFS/AbstractItemContentSubSlave>
// KMime
#include <KMime/Message>

class MessageRfc822SubSlave : public AkonadiFS::AbstractItemContentSubSlave
{
    Q_OBJECT

public:
    MessageRfc822SubSlave(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args);
    ~MessageRfc822SubSlave() override;

public: // AkonadiFS::AbstractItemContentSubSlave API
    void listDir(const AkonadiFS::Uri &url) override;
    void stat(const AkonadiFS::Uri &url) override;
    void get(const AkonadiFS::Uri &url) override;

    bool supportsItemType(const QString &mimeType) const override;

private:
    bool updateMessage(Akonadi::Item::Id itemId);

    KIO::UDSEntry entryForAttachment(KMime::Content *attachment, int index);

private:
    Akonadi::Item::Id mItemId = -1;
    KMime::Message::Ptr mMessage;
};

#endif
