/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef CALENDARINCIDENCESUBSLAVE_H
#define CALENDARINCIDENCESUBSLAVE_H

// AkonadiFS
#include <AkonadiFS/AbstractItemContentSubSlave>
// KF
#include <KCalendarCore/Incidence>

class CalendarIncidenceSubSlave : public AkonadiFS::AbstractItemContentSubSlave
{
    Q_OBJECT

public:
    CalendarIncidenceSubSlave(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args);
    ~CalendarIncidenceSubSlave() override;

public: // AkonadiFS::AbstractItemContentSubSlave API
    void listDir(const AkonadiFS::Uri &url) override;
    void stat(const AkonadiFS::Uri &url) override;
    void get(const AkonadiFS::Uri &url) override;

    bool supportsItemType(const QString &mimeType) const override;

private:
    bool updateMessage(Akonadi::Item::Id itemId);

    KIO::UDSEntry entryForAttachment(const KCalendarCore::Attachment &attachment, int index);

private:
    Akonadi::Item::Id mItemId = -1;
    KCalendarCore::Incidence::Ptr mIncidence;
};

#endif
