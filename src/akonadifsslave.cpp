/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "akonadifsslave.h"

// lib
#include "akonadifsslave_log.h"
// AkonadiFS
#include <AkonadiFS/AbstractItemContentSubSlave>
// Akonadi
#include <Akonadi/CollectionFetchJob>
#include <Akonadi/EntityDisplayAttribute>
#include <Akonadi/ItemFetchJob>
#include <Akonadi/ItemFetchScope>
// KF
#include <KPluginMetaData>
#include <KPluginFactory>
#include <KAboutData>
#include <KLocalizedString>
// Qt
#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>

#ifdef Q_OS_WIN
// see kio/core/src/kioglobal_p.h
#define S_IRUSR 0400
#define S_IRGRP 0040
#define S_IROTH 0004
#endif

// Pseudo plugin class to embed meta data
class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.slave.akonadifs" FILE "akonadifs.json")
};

extern "C" {
int Q_DECL_EXPORT kdemain(int argc, char **argv);
}

int kdemain(int argc, char **argv)
{
    QApplication app(argc, argv);

    KAboutData aboutData(QStringLiteral("kio_akonadifs"), QString(), QStringLiteral("0"));
    KAboutData::setApplicationData(aboutData);

    QCommandLineParser parser;
    parser.addOption(QCommandLineOption(QStringList{QStringLiteral("+protocol")}, i18n("Protocol name")));
    parser.addOption(QCommandLineOption(QStringList{QStringLiteral("+pool")}, i18n("Socket name")));
    parser.addOption(QCommandLineOption(QStringList{QStringLiteral("+app")}, i18n("Socket name")));
    aboutData.setupCommandLine(&parser);

    parser.process(app);
    aboutData.processCommandLine(&parser);

    AkonadiFsSlave slave(parser.positionalArguments().at(1).toLocal8Bit(),
                         parser.positionalArguments().at(2).toLocal8Bit());

    slave.dispatchLoop();

    return 0;
}

AkonadiFsSlave::AkonadiFsSlave(const QByteArray &pool_socket, const QByteArray &app_socket)
    : KIO::SlaveBase("akonadifs", pool_socket, app_socket)
{
    const QVariantList args{QVariant::fromValue<KIO::SlaveBase*>(this)};

    const QVector<KPluginMetaData> offers = KPluginMetaData::findPlugins(QStringLiteral("akonadifs"));
    for (const KPluginMetaData &offer : offers) {
        if (auto plugin = KPluginFactory::instantiatePlugin<AkonadiFS::AbstractItemContentSubSlave>(offer, nullptr, args).plugin) {
            mHandlers.append( plugin);
        }
    }

    qCDebug(AKONADIFSSLAVE_LOG) << "kio_akonadi starting up";
}

AkonadiFsSlave::~AkonadiFsSlave()
{
    qCDebug(AKONADIFSSLAVE_LOG) << "kio_akonadi shutting down";
    qDeleteAll(mHandlers);
}

void AkonadiFsSlave::get(const QUrl &url)
{
    const AkonadiFS::Uri akonadiFSUri(url);

    if (akonadiFSUri.isItem()) {
        gettem(akonadiFSUri.itemId());
        return;
    }

    if (akonadiFSUri.isItemSubPath()) {
        AkonadiFS::AbstractItemContentSubSlave *handler = handlerForItem(akonadiFSUri.itemId());
        if (handler) {
            handler->get(akonadiFSUri);
            return;
        }
    }

    error(KIO::ERR_DOES_NOT_EXIST, i18n("Invalid URL."));
}

void AkonadiFsSlave::gettem(Akonadi::Item::Id itemId)
{
    const Akonadi::Item item(itemId);

    auto job = new Akonadi::ItemFetchJob(item);
    job->fetchScope().fetchFullPayload();

    if (!job->exec()) {
        error(KIO::ERR_INTERNAL, job->errorString());
        return;
    }

    if (job->items().count() != 1) {
        error(KIO::ERR_DOES_NOT_EXIST, i18n("No such item."));
        return;
    }

    const Akonadi::Item payLoaditem = job->items().at(0);
    data(payLoaditem.payloadData());
    data(QByteArray());
    finished();
}

void AkonadiFsSlave::stat(const QUrl &url)
{
    qCDebug(AKONADIFSSLAVE_LOG) << "stat" << url;

    const AkonadiFS::Uri akonadiFSUri(url);

    if (akonadiFSUri.isCollection()) {
        statCollection(akonadiFSUri.lastCollectionId());
        return;
    }

    if (akonadiFSUri.isItem()) {
        statItem(akonadiFSUri.itemId());
        return;
    }

    if (akonadiFSUri.isItemSubPath()) {
        AkonadiFS::AbstractItemContentSubSlave *handler = handlerForItem(akonadiFSUri.itemId());
        if (handler) {
            handler->stat(akonadiFSUri);
            return;
        }
    }

    error(KIO::ERR_DOES_NOT_EXIST, i18n("Invalid URL."));
}

void AkonadiFsSlave::statCollection(Akonadi::Collection::Id collectionId)
{
    Akonadi::Collection collection(collectionId);

    if (collection != Akonadi::Collection::root()) {
        // Check that the collection exists.
        auto job = new Akonadi::CollectionFetchJob(collection, Akonadi::CollectionFetchJob::Base);
        if (!job->exec()) {
            error(KIO::ERR_INTERNAL, job->errorString());
            return;
        }

        if (job->collections().count() != 1) {
            error(KIO::ERR_DOES_NOT_EXIST, i18n("No such collection."));
            return;
        }

        collection = job->collections().at(0);
    }

    statEntry(entryForCollection(collection));
    finished();
}

void AkonadiFsSlave::statItem(Akonadi::Item::Id itemId)
{
    const Akonadi::Item item(itemId);

    auto job = new Akonadi::ItemFetchJob(item);

    if (!job->exec()) {
        error(KIO::ERR_INTERNAL, job->errorString());
        return;
    }

    if (job->items().count() != 1) {
        error(KIO::ERR_DOES_NOT_EXIST, i18n("No such item."));
        return;
    }

    const Akonadi::Item fullItem = job->items().at(0);
    statEntry(entryForItem(fullItem));
    finished();
}

void AkonadiFsSlave::listDir(const QUrl &url)
{
    qCDebug(AKONADIFSSLAVE_LOG) << "listDir" << url;

    const AkonadiFS::Uri akonadiFSUri(url);

    if (akonadiFSUri.isCollection()) {
        listCollectionDir(akonadiFSUri.lastCollectionId());
        return;
    }

    if ((akonadiFSUri.isItem()) ||
        (akonadiFSUri.isItemSubPath())) {
        AkonadiFS::AbstractItemContentSubSlave *handler = handlerForItem(akonadiFSUri.itemId());
        if (handler) {
            handler->listDir(akonadiFSUri);
            return;
        }
    }

    error(KIO::ERR_DOES_NOT_EXIST, i18n("Invalid URL."));
}

void AkonadiFsSlave::listCollectionDir(Akonadi::Collection::Id collectionId)
{
    Akonadi::Collection collection(collectionId);

    // Fetching collections
    if (!collection.isValid()) {
        error(KIO::ERR_DOES_NOT_EXIST, i18n("No such collection."));
        return;
    }

qDebug() << "LISTING DIR FOR" << collection.name();
    auto job = new Akonadi::CollectionFetchJob(collection, Akonadi::CollectionFetchJob::FirstLevel);
    if (!job->exec()) {
        error(KIO::ERR_CANNOT_ENTER_DIRECTORY, job->errorString());
        return;
    }
    entryForCollection(collection, true);
    const Akonadi::Collection::List collections = job->collections();
    for (const Akonadi::Collection &col : collections) {
        listEntry(entryForCollection(col));
    }

    // Fetching items
    if (collection != Akonadi::Collection::root()) {
        auto fjob = new Akonadi::ItemFetchJob(collection);
        if (!fjob->exec()) {
            error(KIO::ERR_INTERNAL, job->errorString());
            return;
        }
        const Akonadi::Item::List items = fjob->items();
        totalSize(collections.count() + items.count());
        for (const Akonadi::Item &item : items) {
            listEntry(entryForItem(item));
        }
    }

    finished();
}

AkonadiFS::AbstractItemContentSubSlave *AkonadiFsSlave::handlerForItem(Akonadi::Item::Id itemId) const
{
    // TODO: optimize to use handler cache as well pass item on
    const Akonadi::Item item(itemId);

    auto job = new Akonadi::ItemFetchJob(item);

    if (!job->exec()) {
        return nullptr;
    }

    if (job->items().count() != 1) {
        return nullptr;
    }

    const Akonadi::Item fullItem = job->items().at(0);
    const QString itemMimeType = fullItem.mimeType();
    for (auto handler : mHandlers) {
        if (handler->supportsItemType(itemMimeType)) {
            return handler;
        }
    }
    return nullptr;
}

KIO::UDSEntry AkonadiFsSlave::entryForItem(const Akonadi::Item &item, bool isDotEntry)
{
    KIO::UDSEntry entry;
    entry.reserve(7);
    const QString name = isDotEntry ? QStringLiteral(".") : QString(QLatin1String("item") + QString::number(item.id()));
    entry.fastInsert(KIO::UDSEntry::UDS_NAME, name);
    entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, item.mimeType());
    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
    entry.fastInsert(KIO::UDSEntry::UDS_SIZE, item.size());
    entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR | S_IRGRP | S_IROTH);
    entry.fastInsert(KIO::UDSEntry::UDS_MODIFICATION_TIME, item.modificationTime().toSecsSinceEpoch());
    return entry;
}

KIO::UDSEntry AkonadiFsSlave::entryForCollection(const Akonadi::Collection &collection, bool isDotEntry)
{
    KIO::UDSEntry entry;
    entry.reserve(7);
    const QString name = isDotEntry ? QStringLiteral(".") : QString::number(collection.id());
    entry.fastInsert(KIO::UDSEntry::UDS_NAME, name);
    entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, Akonadi::Collection::mimeType());
    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
    entry.fastInsert(KIO::UDSEntry::UDS_ACCESS, S_IRUSR | S_IRGRP | S_IROTH);
    QString displayName = collection.name();
    if (const auto *attr = collection.attribute<Akonadi::EntityDisplayAttribute>()) {
        if (!attr->iconName().isEmpty()) {
            entry.fastInsert(KIO::UDSEntry::UDS_ICON_NAME, attr->iconName());
        }
        if (!attr->displayName().isEmpty()) {
            displayName = attr->displayName();
        }
    }
    entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, displayName);
    return entry;
}

#include "akonadifsslave.moc"
