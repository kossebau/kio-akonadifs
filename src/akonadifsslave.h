/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADIFS_SLAVE_H
#define AKONADIFS_SLAVE_H

// Akonadi
#include <Akonadi/Collection>
#include <Akonadi/Item>
// KF
#include <KIO/SlaveBase>
// Qt
#include <QVector>

namespace AkonadiFS {
class AbstractItemContentSubSlave;
}

class AkonadiFsSlave : public KIO::SlaveBase
{
    friend class AkonadiFS::AbstractItemContentSubSlave;

public:
    AkonadiFsSlave(const QByteArray &pool_socket, const QByteArray &app_socket);
    ~AkonadiFsSlave() override;

public: // KIO::SlaveBase API
    void get(const QUrl &url) override;
    void stat(const QUrl &url) override;
    void listDir(const QUrl &url) override;

private:
    void statCollection(Akonadi::Collection::Id collectionId);
    void statItem(Akonadi::Item::Id itemId);
    void gettem(Akonadi::Item::Id itemId);
    void listCollectionDir(Akonadi::Collection::Id collectionId);

    AkonadiFS::AbstractItemContentSubSlave *handlerForItem(Akonadi::Item::Id itemId) const;

private:
    static KIO::UDSEntry entryForItem(const Akonadi::Item &item, bool isDotEntry = false);
    static KIO::UDSEntry entryForCollection(const Akonadi::Collection &collection, bool isDotEntry = false);

private:
    QVector<AkonadiFS::AbstractItemContentSubSlave*> mHandlers;
};

#endif
