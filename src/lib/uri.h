/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADIFS_URI_H
#define AKONADIFS_URI_H

// lib
#include "akonadifs_export.h"
// Akonadi
#include <Akonadi/Collection>
#include <Akonadi/Item>
// Qt
#include <QUrl>
#include <QVector>

namespace AkonadiFS {

// akonadifs:///[<collectionid>/[...]][/item<id>[/<subpath>]]
class AKONADIFS_EXPORT Uri
{
 public:
    enum Type { InvalidUrl, Collection, Item, ItemSubPath};
 public:
    explicit Uri(const QUrl &url);

 public:
    Akonadi::Collection::Id lastCollectionId() const              { return mCollectionIds.last(); }
    const QVector<Akonadi::Collection::Id> &collectionIds() const { return mCollectionIds; }
    Akonadi::Item::Id itemId() const                              { return mItemId; }
    const QString& itemSubPath() const                            { return mItemSubPath; }
    Uri::Type type() const;
    bool isCollection() const                                     { return type() == Collection; }
    bool isItem() const                                           { return type() == Item; }
    bool isItemSubPath() const                                    { return type() == ItemSubPath; }
    bool isValid() const                                          { return type() != InvalidUrl; }

private: // data
    QVector<Akonadi::Collection::Id> mCollectionIds;
    Akonadi::Item::Id mItemId = -1;
    QString mItemSubPath;
};

}

#endif
