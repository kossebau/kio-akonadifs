/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "abstractitemcontentsubslave.h"

namespace AkonadiFS {

class AbstractItemContentSubSlavePrivate
{
public:
    KIO::SlaveBase * const mSlave;
};

AbstractItemContentSubSlave::AbstractItemContentSubSlave(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args)
    : QObject(parent)
    , d(new AbstractItemContentSubSlavePrivate{args.first().value<KIO::SlaveBase*>()})
{
    Q_UNUSED(metaData);
}

AbstractItemContentSubSlave::~AbstractItemContentSubSlave() = default;

void AbstractItemContentSubSlave::error(int _errid, const QString &_text)
{
    d->mSlave->error(_errid, _text);
}

void AbstractItemContentSubSlave::listEntry(const KIO::UDSEntry &entry)
{
    d->mSlave->listEntry(entry);
}

void AbstractItemContentSubSlave::statEntry(const KIO::UDSEntry &entry)
{
    d->mSlave->statEntry(entry);
}

void AbstractItemContentSubSlave::data(const QByteArray &data)
{
    d->mSlave->data(data);
}

void AbstractItemContentSubSlave::mimeType(const QString &mimeType)
{
    d->mSlave->mimeType(mimeType);
}

void AbstractItemContentSubSlave::redirection(const QUrl &url)
{
    d->mSlave->redirection(url);
}

void AbstractItemContentSubSlave::finished()
{
    d->mSlave->finished();
}

}
