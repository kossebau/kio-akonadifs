/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADIFS_ABSTRACTITEMCONTENTSUBSLAVE_H
#define AKONADIFS_ABSTRACTITEMCONTENTSUBSLAVE_H

// lib
#include "akonadifs_export.h"
#include "uri.h"
// KF
#include <KIO/SlaveBase>
// Qt
#include <QObject>
#include <QVariantList>
// Std
#include <memory>

namespace KIO {
class UDSEntry;
}

class KPluginMetaData;
class AkonadiFsSlave;

namespace AkonadiFS {

class AKONADIFS_EXPORT AbstractItemContentSubSlave : public QObject
{
    Q_OBJECT

    friend class AkonadiFsSlave;

public:
    AbstractItemContentSubSlave(QObject *parent, const KPluginMetaData &metaData, const QVariantList &args);
    ~AbstractItemContentSubSlave() override;

public: // SubSlave API
    virtual void listDir(const AkonadiFS::Uri &url) = 0;
    virtual void stat(const AkonadiFS::Uri &url) = 0;
    virtual void get(const AkonadiFS::Uri &url) = 0;

    virtual bool supportsItemType(const QString &mimeType) const = 0;

protected:
    void listEntry(const KIO::UDSEntry &entry);
    void statEntry(const KIO::UDSEntry &entry);
    void data(const QByteArray &data);
    void mimeType(const QString &mimeType);
    void redirection(const QUrl &url);
    void error (int _errid, const QString &_text);
    void finished();

private:
    std::unique_ptr<class AbstractItemContentSubSlavePrivate> d;
};

}

// needed for passing in via the constructor's QVariantList args
Q_DECLARE_METATYPE(KIO::SlaveBase*)

#endif
