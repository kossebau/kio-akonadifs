/*
    SPDX-FileCopyrightText: 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "uri.h"

// Qt
#include <QDebug>

namespace AkonadiFS {

Uri::Uri(const QUrl &url)
{
qDebug() << "Parsing url" << url;
    if (!url.isValid() || url.scheme() != QLatin1String("akonadifs")) {
        return;
    }

    const QString path = url.path();
    if (path.isEmpty()) {
        mCollectionIds.append(Akonadi::Collection::root().id());
        return;
    }
    if (!path.startsWith(QLatin1Char('/'))) {
        // not tolerating that, invalid
        return;
    }

    const QLatin1String itemPrefix("item");
qDebug() << "collection id:" << Akonadi::Collection::root().id();
    mCollectionIds.append(Akonadi::Collection::root().id());
    int currentPos = 1;
    int separatorIndex = path.indexOf(QLatin1Char('/'), currentPos);
    bool isEndReached = (separatorIndex == -1);
    if (isEndReached) {
        separatorIndex = path.size();
    }
    while (currentPos < path.size()) {
        const int sectionLength = separatorIndex - currentPos;
        // skip empty sections, tolerating them
        if (sectionLength > 0) {
            const QStringView section = QStringView(path).mid(currentPos, sectionLength);
            if (section.startsWith(itemPrefix)) {
                bool ok = false;
                mItemId = section.mid(itemPrefix.size()).toLongLong(&ok);
                if (ok) {
                    mItemSubPath = path.mid(separatorIndex + 1);
qDebug() << "item id:" << mItemId;
qDebug() << "item subpath:" << mItemSubPath;
                } else {
                    mCollectionIds.clear();
                    mItemId = -1;
                }
                return;
            }
            bool ok = false;
            const Akonadi::Collection::Id id = section.toLongLong(&ok);
            if (!ok) {
                mCollectionIds.clear();
                return;
            }
qDebug() << "collection id:" << id;
            mCollectionIds.append(id);
        }
        currentPos = separatorIndex + 1;
        separatorIndex = path.indexOf(QLatin1Char('/'), currentPos);
        isEndReached = (separatorIndex == -1);
        if (isEndReached) {
            separatorIndex = path.size();
        }
    }
}

Uri::Type Uri::type() const
{
    Type result =
        mCollectionIds.isEmpty() ? InvalidUrl :
        (mItemId < 0) ?            Collection :
        mItemSubPath.isEmpty() ?   Item :
        /*else*/                   ItemSubPath;

    return result;
}

}
